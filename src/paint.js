(function () {

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    /**
     * @param {[][]string} grid The grid on which you should implement the flooding algorithm using the color defined at the x and y coordinates given in the x and y args
     * @param {number} x The x coordinate
     * @param {number} y The y coordinate
     * @param {string} color The color to fill with using the flood algorithm
     * @return {[][]string} The grid array with the implemented flood fill algorithm
     */
    //Function which you have to implement
    function floodFillAt(grid, x, y, color) {
        const target = grid[y][x];
        if (target === color) {
            return grid;
        }
        const h = grid.length;
        const w = grid[0].length;
        let queue = [];
        grid[y][x] = color;
        queue.push(y, x);
        while(queue.length > 0) {
            let r = queue[0];
            let c = queue[1];
            if (r + 1 < h && grid[r+1][c] === target) {
                grid[r+1][c] = color;
                queue.push(r+1, c);
            }
            if (r - 1 >= 0 && grid[r-1][c] === target) {
                grid[r-1][c] = color;
                queue.push(r-1, c);
            }
            if (c + 1 < w && grid[r][c+1] === target) {
                grid[r][c+1] = color;
                queue.push(r, c+1);
            }
            if (c - 1 >= 0 && grid[r][c-1] === target) {
                grid[r][c-1] = color;
                queue.push(r, c-1);
            }
            if (r + 1 < h && c + 1 < w && grid[r+1][c+1] === target) {
                grid[r+1][c+1] = color;
                queue.push(r+1, c+1);
            }
            if (r + 1 < h && c - 1 >= 0 && grid[r+1][c-1] === target) {
                grid[r+1][c-1] = color;
                queue.push(r+1, c-1);
            }
            if (r - 1 >= 0 && c + 1 < w  && grid[r-1][c+1] === target) {
                grid[r-1][c+1] = color;
                queue.push(r-1, c+1);
            }
            if (r - 1 >= 0 && c - 1 >= 0 && grid[r-1][c-1] === target) {
                grid[r-1][c-1] = color;
                queue.push(r-1, c-1);
            }
            queue.shift();
            queue.shift();
        }
        return grid;        
    }

    //Generates a (rowsxcolumns) grid where the color of each point in the grid is randomly selected to be red, blue or green
    function generateRandomGrid(colors, rows, columns) {
        //The colors this grid allows
        var COLORS = colors;

        //The number of columns in this grid
        var GRID_NUMBER_COLUMNS = rows;
        //The number of rows in this grid
        var GRID_NUMBER_ROWS = columns;

        //The 2D array which will be used to store the randomly generated color values
        var grid = [];

        //Move row by row and populate each point in the row with a random color
        for(var rowIndex=0; rowIndex<GRID_NUMBER_ROWS; rowIndex++) {
            //Create the array at this row which represents the column
            grid[rowIndex] = [];

            //Go through each point in the column
            for(var columnIndex=0; columnIndex<GRID_NUMBER_COLUMNS; columnIndex++) {
                //Generate the random color for the point at rowIndex,columnIndex
                var colorForCurrentCoord = COLORS[getRandomInt(0, colors.length - 1)];

                //Set the color
                grid[rowIndex][columnIndex] = colorForCurrentCoord;
            }
        }

        //Return the generated grid
        return grid;
    }


    // validates the hex color inputs and adds the error class if invalid
    function validateInputs() {
        let valid = true;        
        document.querySelectorAll('#colors input').forEach(input => {
            if (/^[0-9A-Fa-f]{6}$/.test(input.value)) {
                input.classList.remove('error');
            } else {
                input.classList.add('error');
                valid = false;
            }
        });
        return valid;
    }

    // converts hex codes to RGB objects
    function hexToRgb(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

    // converts grid data to canvas ImageData
    function makeImageDataFromGrid(grid, imagedata) {
        const height = grid.length;
        const width = grid[0].length;
        for (let r = 0; r < height; r++) {
            for (let c = 0; c < width; c++) {
                const rgb = hexToRgb(grid[r][c]);
                const offset = (r * width + c) * 4;
                imagedata.data[offset] = rgb.r;     // Red
                imagedata.data[offset + 1] = rgb.g; // Green
                imagedata.data[offset + 2] = rgb.b;  // Blue
                imagedata.data[offset + 3] = 255;   // Alpha
            }
        }
    }

    // creates the canvas and adds it to the DOM
    function initCanvas(width, height) {
        document.querySelector('#canvas_div').innerHTML = `<canvas id="canvas" width="${width}" height="${height}"></canvas>`;
    }


    // sets the canvas color data
    function setCanvasData(grid, width, height) {
        const canvas = document.querySelector('#canvas');
        const context = canvas.getContext("2d");
        const imagedata = context.createImageData(width, height);
        makeImageDataFromGrid(grid, imagedata);
        context.putImageData(imagedata, 0, 0);
    }

    // gets the click coordinates on the canvas
    function getCursorPosition(canvas, event) {
        const rect = canvas.getBoundingClientRect();
        const x = event.clientX - rect.left;
        const y = event.clientY - rect.top;
        return [x, y];
    }

    document.querySelector('#generate').addEventListener('click', () => {
        const validInputs = validateInputs();
        if (!validInputs) {
            return;
        }
        [fillColor, ...palette] = [...document.querySelectorAll('#colors input')].map(input => input.value);
        [width, height] = [...document.querySelectorAll('#size input')].map(input => Number(input.value));
        let grid = generateRandomGrid(palette, width, height);
        initCanvas(width, height);
        setCanvasData(grid, width, height);

        const canvas = document.querySelector('canvas');
        canvas.addEventListener('click', function(e) {
            [x, y] = getCursorPosition(canvas, e);
            x = Math.floor(x);
            y = Math.floor(y); 
            grid = floodFillAt(grid, x, y, fillColor);
            setCanvasData(grid, width, height);
        });
    });
})();
